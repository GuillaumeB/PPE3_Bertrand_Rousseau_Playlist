from setuptools import setup

setup(
	name="generator",
	version="1.0",
	author="Bertrand Guillaume Rousseau Kevin",
	description="description test setuptools",
	license="GPLv3",
	url='https://framagit.org/GuillaumeB/PPE3_Bertrand_Rousseau_Playlist',
    	install_requires=['psycopg2'],
	packages=['programme'],
	entry_points=
     {
		'console_scripts':[
			'generator = programme.generator:main'
		]
	}
      
)

