# -*- coding: utf-8 -*-
"""
Created on Tue Sep 27 17:45:56 2016

@author: etudiant
"""

import logging
logger = logging.getLogger("album")
logger.setLevel(logging.DEBUG)

class Album(object):
    def __init__(self, _title, _year, _nbTrack):
        """Constructors of Album class
        """
        logger.debug("Launching album")
        self.title = str()
        self.year = int()
        self.nbTrack = int()
        self.title = _title
        self.year = _year
        self.nbTrack = _nbTrack
        
    def getTitle(self):
        """Return album title as a str()"""
        return self.title
    def setTitle(self, _title):
        """Set album name using the given parameter str()"""
        self.title = str(_title)
        logger.debug("Album title of channel is now " + self.title)
        
    def getYear(self):
        """Return playlist year as a int()"""
        return self.year
    def setYear(self, _year):
        """Set album year using the given parameter int()"""
        self.year = int(_year)
        logger.debug("Album year of channel is now " + str(self.year))
    
    def getNbTrack(self):
        """Return number of track as a int()
        """
        return self.nbTrack
    def setNbTrack(self, _nbTrack):
        """Set number of track using the given parameter int()"""
        self.nbTrack = int(_nbTrack)
        logger.debug("Album number of track of channel is now " + str(self.nbTrack))
    

if __name__ == "__main__":
    logger.debug("Execution album.py")
    al = Album("titre", 1000, 145)
    al.setTitle("Titre Album")
    al.setYear(2000)
    al.setNbTrack(10)
    logger.debug("End of execution album.py")
else:
    logger.debug("Loading of album")
    