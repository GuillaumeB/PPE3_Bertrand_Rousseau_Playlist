# -*- coding: utf-8 -*-
"""
Created on Tue Sep 27 18:44:12 2016

@author: k.rousseau
"""

import logging
logger = logging.getLogger("artist")
logger.setLevel(logging.DEBUG)

class Artist(object):
    def __init__(self, _name, _firstname, _age, _nbr_album, _kind):
        """ Constructor of the class artist """
        logger.debug("Instantiation of an 'artist'") 
        self.name = str()
        self.firstname = str()
        self.age = int()
        self.nbr_album = int()
        self.kind = str()
        self.name = _name
        self.firstname = _firstname
        self.age = _age
        self.nbr_album = _nbr_album
        self.kind = _kind
        
    
    def getName(self):
        """Return artist name as a str()"""
        return self.name
    
    def setName(self, _name):
        """Set artist name using the given parameter str()"""
        self.name = str(_name)
        logger.debug("Artist name is now " + self.name)
    
    def getFirstname(self):
        """Return artist firstname as an str()"""
        return self.firstname
    def setFirstname(self, _firstname):
        """Set artist firstname using the given parameter int()"""
        self.firstname = str(_firstname)
        logger.debug("Artist firstname of channel is now " + self.firstname)
    
    def getAge(self):
        """Return artist age as a str()"""
        return self.age
    
    def setAge(self, _age):
        """Set artist age using the given parameter str()"""
        self.age = int(_age)
        logger.debug("Artist age is now " + str(self.age))
    
    def getNbr_album(self):
        """Return artist nbr_album as a str()"""
        return self.nbr_album
    
    def setNbr_album(self, _nbr_album):
        """Set artist nbr_album using the given parameter str()"""
        self.nbr_album = int(_nbr_album)
        logger.debug("Artist number of album is now " + str(self.nbr_album))
    
    def getKind(self):
        """Return artist kind as a str()"""
        return self.kind
    
    def setKind(self, _kind):
        """Set artist kind using the given parameter str()"""
        self.kind = str(_kind)
        logger.debug("Artist kind is now " + self.kind)
    
if __name__ == "__main__":
    logger.debug("Execution of the 'artist' Module")
    a = Artist("name","firstname",9,0,"Kind")
    a.setName("name")
    a.setFirstname("new firstname")
    a.setAge(10)
    a.setNbr_album(1)
    a.setKind("new Kind")
    logger.debug("End of execution of the 'artist' Module")
    
else:
    logger.debug("Loading 'artist' Module")