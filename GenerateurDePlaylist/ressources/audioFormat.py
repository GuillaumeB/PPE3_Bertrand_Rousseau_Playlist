# -*- coding: utf-8 -*-
"""
Created on Wed Sep 28 17:17:04 2016

@author: etudiant
"""

import logging
logger = logging.getLogger("audioFormat")
logger.setLevel(logging.DEBUG)

class AudioFormat(object):
    def __init__(self, _name):
        """ Constructor of the class audioFormat """
        logger.debug("Instantiation of a 'audioFormat'")
        self.name = str()
        self.name = _name
        
    def getName(self):
        """Return audioFormat name as a str()"""
        return self.name
    
    def setName(self, _name):
        """Set kind name using the given parameter str()"""
        self.name = str(_name)
        logger.debug("The new audioFormat name is " + self.name + ".")
    
if __name__ == "__main__":
    logger.debug("Execution audioFormat")
    af = AudioFormat("name audioformat")
    af.setName("new audio Format")
    logger.debug("End Execution audioFormat")
    
else:
    logger.debug("Loading audioFormat")