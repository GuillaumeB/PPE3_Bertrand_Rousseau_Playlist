Create table "guillaume_kevin".track (
id integer primary key,
title varchar(20),
numTrack integer,
duration integer,
pathT varchar(30));

Create table "guillaume_kevin".artist (
id integer primary key,
name varchar(20),
firstname varchar(20),
age integer,
NbrAlbum integer,
kind varchar(15));

Create table "guillaume_kevin".album (
id integer primary key,
title varchar(20),
year integer,
nbtrack integer);

Create table "guillaume_kevin".playlist (
id integer primary key,
title varchar(20),
duration integer,
criterion varchar(15));

Create table "guillaume_kevin".kind (
id integer primary key,
name varchar(20));

Create table "guillaume_kevin".format (
id integer primary key,
name varchar(20));

Create table "guillaume_kevin".polyphony (
id integer primary key,
nbchannel integer,
description varchar(30));

Create table "guillaume_kevin".trackplaylist (
idTrack integer not null,
idPlaylist integer not null,
primary key(idTrack, idPlaylist),
foreign key (idTrack) references "guillaume_kevin".Track(id),
foreign key (idPlaylist) references "guillaume_kevin".Playlist(id));

alter table "guillaume_kevin".track
add constraint fk_track
foreign key (id) references "guillaume_kevin".format(id);

insert into "guillaume_kevin".artist values
(1,'Guillaume','Bertrand',19,1,'Nightcore');
insert into "guillaume_kevin".artist values
(2,'Kevin','Rousseau',19,10,'Rap');

insert into "guillaume_kevin".album values
(1,'Album1',1997,10);
insert into "guillaume_kevin".album values
(2,'Album2',1997,20);

insert into "guillaume_kevin".format values
(1,'Format1');
insert into "guillaume_kevin".format values
(2,'Format2');

insert into "guillaume_kevin".kind values
(1,'Kind1');
insert into "guillaume_kevin".kind values
(2,'Kind2');

insert into "guillaume_kevin".playlist values
(1,'Playlist1',100,'relax man');
insert into "guillaume_kevin".playlist values
(2,'Playlist2',200,'for gamers');

insert into "guillaume_kevin".polyphony values
(1,2,'2 channels');
insert into "guillaume_kevin".polyphony values
(2,3,'3 channels');

insert into "guillaume_kevin".track values
(1,'Track1',1,180,'Pathtrack1');
insert into "guillaume_kevin".track values
(2,'Track2',2,200,'Pathtrack2');

