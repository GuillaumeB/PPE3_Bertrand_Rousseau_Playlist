# -*- coding: utf-8 -*-
"""
Created on Wed Sep 28 18:04:21 2016

@author: etudiant
"""

import logging
logger = logging.getLogger("kind")
logger.setLevel(logging.DEBUG)

class Kind(object):
    def __init__(self, _name):
        """ Constructor of the class kind """
        logger.debug("Instantiation of a 'kind'") 
        self.name = str()
        self.name = _name
    
    def getName(self):
        """Return kind name as a str()"""
        return self.name
    
    def setName(self, _name):
        """Set kind name using the given parameter str()"""
        self.name = str(_name)
        logger.debug("The new kind name is " + self.name + ".")
        
    
if __name__ == "__main__":
    logger.debug("Execution of the 'kind' Module")
    k = Kind("Kind of album")
    k.setName("Kind name 'Cool'")
    logger.debug("End of execution of the 'kind' Module")
    
else:
    logger.debug("Loading 'kind' Module")