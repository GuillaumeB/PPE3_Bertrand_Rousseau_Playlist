# -*- coding: utf-8 -*-
"""
Created on Wed Sep 28 18:40:45 2016

@author: etudiant
"""

import logging
logger = logging.getLogger("m3u")
logger.setLevel(logging.DEBUG)

class M3u(object):
    def __init__(self):
        pass
    
if __name__ == "__main__":
    logger.debug("Execution du module 'm3u'")
    m = M3u()
    logger.debug("Fin d'execution du module 'm3u'")
    
else:
    logger.debug("Chargement du module 'm3u'")