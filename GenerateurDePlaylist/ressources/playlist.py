# -*- coding: utf-8 -*-
"""
Created on Tue Sep 27 16:51:25 2016

@author: etudiant
"""

import logging
logger = logging.getLogger("playlist")
logger.setLevel(logging.DEBUG)

class Playlist(object):
    def __init__(self, _title, _duration, _criterion):
        """ Constructeur de la classe Playlist """
        logger.debug("Instanciation d'un 'Playlist'") 
        self.title = str()
        self.duration = int()
        self.criterion = str()
        self.title = _title
        self.duration = _duration
        self.criterion = _criterion
    
    def getTitle(self):
        """Return playlist title as a str()"""
        return self.title
    
    def setTitle(self, _title):
        """Set playlist title using the given parameter str()"""
        self.title = str(_title)
        logger.debug("Playlist title is now " + self.title)
    
    def getDuration(self):
        """Return playlist duration as an int()"""
        return self.duration
    
    def setDuration(self, _duration):
        """Set playlist duration using the given parameter int()"""
        self.duration = int(_duration)
        logger.debug("Playlist duration is now " + str(self.duration))
    
    def getCriterion(self):
        """Return playlist criterion as a str()"""
        return self.criterion
    
    def setCriterion(self, _criterion):
        """Set playlist criterion using the given parameter str()"""
        self.criterion = str(_criterion)
        logger.debug("Playlist criterion is now " + self.criterion)
        
    def add(self):
        pass
    
    def delete(self):
        pass
    
    def rename(self):
        pass
    
    def order(self):
        pass
    
    def export(self):
        pass
    
if __name__ == "__main__":
    logger.debug("Execution du module 'Playlist'")
    p = Playlist("title",5,"criterion")
    p.setTitle("new title of playlist")
    p.setDuration(10)
    p.setCriterion("new criterion of playlist")
    logger.debug("Fin d'execution du module 'Playlist'")
    
else:
    logger.debug("Chargement du module 'Playlist'")