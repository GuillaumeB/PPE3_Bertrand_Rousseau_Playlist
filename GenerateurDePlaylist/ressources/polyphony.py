# -*- coding: utf-8 -*-
"""
Created on Wed Sep 28 16:02:55 2016

@author: etudiant
"""

import logging
logger = logging.getLogger("polyphony")
logger.setLevel(logging.DEBUG)

class Polyphony(object):
    def __init__(self, _numberofchannel, _description):
        """Constructors of polyphony class
        """
        logger.debug("Launching album")
        self.numberofChannel = int()
        self.description = str()
        self.numberofChannel = _numberofchannel
        self.description = _description
        
    def getNumberOfChannel(self):
        """Return number of channel as int
        """
        return self.numberofChannel
    def setNumberOfChannel(self, _numberofchannel):
        """Set number of channel using the given parameter int()
        """
        self.numberofChannel = int(_numberofchannel)
        logger.debug("Polyphony number of channel is now " + str(self.numberofChannel))
    
    def getDescription(self):
        """Return description of kind as str()
        """
        return self.description
    def setDescription(self, _description):
        """Set description of kind using the given parameter str()
        """
        self.description = str(_description)
        logger.debug("Polyphony description is now : " + self.description)

if __name__ == "__main__":
    logger.debug("Execution polyphony.py")
    poly = Polyphony(2, "description")
    poly.setNumberOfChannel(3)
    poly.setDescription("new Description of polyphony")
    logger.debug("End of execution polyphony.py")
else:
    logger.debug("Loading of polyphony")