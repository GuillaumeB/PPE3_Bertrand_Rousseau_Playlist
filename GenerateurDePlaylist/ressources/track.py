# -*- coding: utf-8 -*-
"""
Created on Tue Sep 27 16:26:50 2016

@author: etudiant
"""

import artist
import kind
import album
import audioFormat
import polyphony

import logging
logger = logging.getLogger("track")
logger.setLevel(logging.DEBUG)

class Track(object):
    def __init__(self, _title, _numtrack, _duration, _path, _album, _kind, _artist, _audioFormat, _polyphony):
        """Constructors of Track class
        """
        logger.debug("Launching track")
        self.title = str()
        self.numtrack = int()
        self.duration = int()
        self.path = str()
        self.album=_album
        self.kind=_kind
        self.artist=_artist
        self.audioFormat=_audioFormat
        self.polyphony=_polyphony
        
    def getTitle(self):
        """Return track title as a str
        """
        return self.title

    def setTitle(self, _title):
        """Set track title using the given str()
        """
        self.title = str(_title)
        logger.debug("Track title is now : " + self.title)
        
    def getNumTrack(self):
        """Return num track as an int
        """
        return self.numtrack
        
    def setNumTrack(self, _numtrack):
        """Set track num using the given int
        """
        self.numtrack = int(_numtrack)
        logger.debug("Track numtrack is now " + str(self.numtrack))
    
    def getDuration(self):
        """Return duration as an int
        """
        return self.duration
         
    def setDuration(self, _duration):
        """Set track duration using the given int
        """
        self.duration = int(_duration)
        logger.debug("Track duration is now " + str(self.duration))
     
    def getPath(self):
        """Return path and filename as a str
        """
        return self.path
    
    def setPath(self, _path):
        """Set track path using the given str()
        """
        self.path = str(_path)
        logger.debug("Track path is now " + self.path)
     
    def getAlbum(self):
         """Return the album
         """
         return self.album
         
    def setAlbum(self, _album):
        """Set track album
        """
        self.album = _album
        
    def getKind(self):
         """Return the kind
         """
         return self.kind
         
    def setKind(self, _kind):
        """Set track kind
        """
        self.kind = _kind
        
    def getArtist(self):
         """Return the artist
         """
         return self.artist
         
    def setArtist(self, _artist):
        """Set track artist
        """
        self.artist = _artist
        
    def getAudioFormat(self):
         """Return the audio format
         """
         return self.audioFormat
         
    def setAudioFormat(self, _audioFormat):
        """Set track audio Format
        """
        self.audioFormat = _audioFormat
        
    def getPolyphony(self):
         """Return the polyphony
         """
         return self.polyphony
         
    def setPolyphony(self, _polyphony):
        """Set track polyphony
        """
        self.polyphony = _polyphony
    

if __name__ == "__main__":
    logger.debug("Execution track.py")
    myalbum=album.Album("My first album",1997,10)
    mykind=kind.Kind("strange")
    myartist=artist.Artist("David", "Gregory", 30, 2, "strange")
    myaudioformat=audioFormat.AudioFormat("audioFormat")
    mypolyphony=polyphony.Polyphony(3,"description of polyphony")
    
    
    t = Track("title track",2,100,"Path",myalbum, mykind, myartist, myaudioformat, mypolyphony)    
    t.setTitle("Test title")
    t.setNumTrack(10)
    t.setDuration(20)
    t.setPath("Test path")
    t.getAlbum().setTitle("new new title")
    logger.debug("End Execution track.py")
else:
    logger.debug("Loading track")
    