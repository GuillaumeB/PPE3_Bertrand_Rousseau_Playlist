# -*- coding: utf-8 -*-
"""
Created on Tue Sep 27 15:55:08 2016

@author: guillaume b/kevin r
"""

#import softwares needed for the program
import argparse
import logging
import psycopg2
import sys
from random import shuffle #for the random
from lxml import etree #for xspf

#initializing logger
logger = logging.getLogger("generator")
logger.setLevel(logging.DEBUG)

#initialize arguments
parser = argparse.ArgumentParser()

#create arguments
parser.add_argument('-ar','--artist',help='permit to inform the artist you want and after the percent', action='append', nargs=2)
parser.add_argument('-al','--album',action='append', help='permit to inform the album you want and after the percent', nargs=2)
parser.add_argument('-k','--kind',action='append', help='permit to inform the kind you want and after the percent', nargs=2)
parser.add_argument('-ti','--title',action='append', help='permit to inform the title you want and after the percent', nargs=2)
parser.add_argument('-d','--duration',default =50, help='permit to inform the duration you want', nargs=1)
parser.add_argument('--m3u',action='store_true',help='if you want a m3u file for your playlist')
parser.add_argument('--xspf',action='store_true',help='if you want a xspf file for your playlist')
parser.add_argument('--r',action='store_true',help='permit to mix the playlist at end')

#test for see the differents args (and if they are used by user or no)
args = parser.parse_args()
#print(args)

#main
def main():
    logger.debug("Entry in main")
    
    #connection to database radio_libre for use it
    conn = psycopg2.connect(database="radio_libre", user="g.bertrand", password="P@ssword", host="172.16.99.2", port="5432")
    cursor = conn.cursor()   

    #initialize the percent
    percenttotal = 0
    
    #CHECK percent
    print("======================")
    #For artist(s)
    if args.artist!=None:#if artist is different to default
        i=0
        for arg in args.artist:#for all artist asked by user
            #request in database all the artist by her firstname
            cursor.execute("set schema 'guillaume_kevin'; SELECT * FROM artist WHERE firstname = '"+format(args.artist[i][0])+"'");
            x = cursor.fetchall();
            #print(x)    print the artist with their caracteristic (id/firstname/lastname/age/nbralbum)
            listx = [()] #initilaize a list empty
            if(cursor.fetchall() == listx): #check the result of request isnt null
                print("Please use an artist valid")#ask an artist valid
                sys.exit(0)#leave program
            percenttotal= percenttotal + int(format(args.artist[i][1]))#take the percent for argument and add if there is more than 1 arg for artist
            i=i+1
    if args.album!=None:#if album is different to default   --> similar as artist
        i=0
        for arg in args.album:
            cursor.execute("set schema 'guillaume_kevin'; SELECT * FROM album WHERE title = '" + format(args.album[i][0]) + "'");
            y = cursor.fetchall();
            listx = [()]
            if(cursor.fetchall() == listx):
                print("Please use a title album valid")
                sys.exit(0)
            percenttotal= percenttotal + int(format(args.album[i][1]))
            i=i+1
    if args.kind!=None:#if kind is different to default --> similar as artist
        i=0
        for arg in args.kind:
            cursor.execute("set schema 'guillaume_kevin'; SELECT * FROM kind WHERE name = '"+format(args.kind[i][0])+"'");
            z = cursor.fetchall();
            listx = [()]
            if(cursor.fetchall() == listx):
                print("Please use a kind valid")
                sys.exit(0)
            percenttotal= percenttotal + int(format(args.kind[i][1]))
            i=i+1 
    if args.title!=None:#if title is different to default   --> similar as artist
        i=0
        for arg in args.title:
            cursor.execute("set schema 'guillaume_kevin'; SELECT * FROM track WHERE title = '"+format(args.title[i][0])+"'");
            a = cursor.fetchall(); 
            listx = [()]
            if(cursor.fetchall() == listx):
                print("Please use a title valid")
                sys.exit(0)
            percenttotal= percenttotal + int(format(args.title[i][1]))
            i=i+1
            
    #CHECK Duration
    #print("======================")
    #print("Duration : ")#check if the duration is informed or not
    if args.duration!=50:
        duration = int(format(args.duration[0]))#value duration take the value informed by '--duration value'
        #print(duration)
    else:
        duration =50 #default value : 50min or 3000s
        #print(duration)
    
    #CHECK percent
    if percenttotal>100:#if percenttotal > 100 send a message and stop the program 
        print("Error ! You need to enter less than 100 (100 include) !")      
    elif percenttotal<100:#if percenttotal <100
        print("You entered less than 100 percent, the rest of percent will be selected randomly in bdd.")#inform user
        diffpercent = 100 - percenttotal#calculate difference between 100 and the percent total informed
        playList=[]
        #Check if the user use m3u/xspf/or both
        if args.m3u!=True:#if the argument --m3u isnt informed
            #print("no m3u")
            if args.xspf!=True:#if the argument --xspf isnt informed
                print("Please enter a format ! --m3u or --xspf")#m3u and xspf aren't informed so the program close
                sys.exit(0)
                
        if args.xspf!=True:#if the argument --xspf isnt informed
            #print("no xspf")
            if args.m3u!=True:#if the argument --m3u isnt informed
                print("Please enter a format ! --m3u or --xspf")#xspf and m3u aren't informed so the program close
                sys.exit(0)
        
        #if the argument artist is different to default
        if args.artist!=None:
            print("=======ARTIST========")#separate fields
            i=0
            for arg in args.artist:#for all artist informed by user
                print("===")
                print("Artist "+str(i + 1))#separate artist(s)
                print("===")
                #request in database all the track (title/duration/path) for an artist informed by user
                cursor.execute("set schema 'guillaume_kevin'; SELECT track.title, track.duration, track.path FROM track INNER JOIN artist ON track.artist = artist.id WHERE artist.firstname = '"+format(args.artist[i][0])+"'");
                x = cursor.fetchall();
                #print(x)
                shuffle(x)#permit to mix element in a list (here the result of the request)
                #print(x)
                durationArg = ((int(format(args.artist[i][1]))/100)) * duration#convert the arg artist duration into percent(/100) and multipliate by duration total informed by user          
                durationArg = durationArg * 60 #convert duration in s (*60)
                print("duration in second for this arg = "+str(durationArg))#test for see if the arg duration is ok
                timeUse = 0 #initialize the time use for this argument
                j=0
                k=0
                while durationArg > timeUse:#while duration of this argument > timeUsed by tracks selected
                    #request for count number of track for this artist
                    cursor.execute("set schema 'guillaume_kevin'; SELECT count(*) FROM track INNER JOIN artist ON track.artist = artist.id WHERE artist.firstname = '"+format(args.artist[i][0])+"'");
                    z=cursor.fetchone()
                    timeUse= timeUse + x[k][1]#timeUse take value of the duration of the track(s)                                
                    playList.append(x[k])#add track to playlist
                    print(str(timeUse))#test for see the timeUse increase
                    j=j+1
                    k=k+1
                    if k == z[0]:#this condition is use when you don't have any tracks on playlist and need to restart
                        k= 0
                print(" number of music to the artist : "+str(j))#display the nomber of track for this argument
                i=i+1#permit to separate artist 1/artist 2/..
        
        #if the argument album is different to default
        if args.album!=None:
            print("=======ALBUM========")# similar as artist
            i=0
            for arg in args.album:
                print("===")
                print("Album "+str(i + 1))
                print("===")
                #request in database all the track (title/duration/path) for an album informed by user
                cursor.execute("set schema 'guillaume_kevin'; SELECT track.title, track.duration, track.path FROM track INNER JOIN album ON track.album = album.id WHERE album.title = '"+format(args.album[i][0])+"'");
                x = cursor.fetchall();
                shuffle(x)
                #convert the arg album duration into percent(/100) and multipliate by duration total informed by user
                durationArg = ((int(format(args.album[i][1]))/100)) * duration             
                durationArg = durationArg * 60 #convert duration in s (*60)
                print("duration in second for this arg = "+str(durationArg))
                timeUse = 0
                j=0
                k=0
                while durationArg > timeUse:
                    #request for count number of track for this album
                    cursor.execute("set schema 'guillaume_kevin'; SELECT count(*) FROM track INNER JOIN album ON track.album = album.id WHERE album.title = '"+format(args.album[i][0])+"'");
                    z=cursor.fetchone()
                    timeUse= timeUse + x[k][1]                                  
                    playList.append(x[k])#add track to playlist    
                    print(str(timeUse))
                    j=j+1
                    k=k+1
                    if k == z[0]:
                        k= 0
                print(" number of music to the album : "+str(j))#display the number of track for this argument
                i=i+1#permit to separate album 1/album 2/..
        
        #if the argument kind is different to default
        if args.kind!=None:
            print("=======KIND========")#similar as artist
            i=0
            for arg in args.kind:
                print("===")
                print("Kind "+str(i + 1))
                print("===")
                #request in database all the track (title/duration/path) for a kind informed by user
                cursor.execute("set schema 'guillaume_kevin'; SELECT track.title, track.duration, track.path FROM track INNER JOIN kind ON track.kind = kind.id WHERE kind.name = '"+format(args.kind[i][0])+"'");
                x = cursor.fetchall();
                shuffle(x)
                #convert the arg kind duration into percent(/100) and multipliate by duration total informed by user
                durationArg = ((int(format(args.kind[i][1]))/100)) * duration             
                durationArg = durationArg * 60 #convert duration in s (*60)
                print("duration in second for this arg = "+str(durationArg))
                timeUse = 0
                j=0
                k=0
                while durationArg > timeUse:
                    #request for count number of track for this kind
                    cursor.execute("set schema 'guillaume_kevin'; SELECT count(*) FROM track INNER JOIN kind ON track.kind = kind.id WHERE kind.name = '"+format(args.kind[i][0])+"'");
                    z=cursor.fetchone()
                    timeUse= timeUse + x[k][1]                                
                    playList.append(x[k])#add track to playlist
                    print(str(timeUse))
                    j=j+1
                    k=k+1
                    if k == z[0]:
                        k= 0
                print(" number of music with the kind : "+str(j))#display the number of track for this argument
                i=i+1#permit to separate kind 1/kind 2/..
        
        #if the argument title is different to default
        if args.title!=None:
            print("=======TITLE========")
            i=0
            for arg in args.title:
                print("===")
                print("Title "+str(i + 1))
                print("===")
                #request in database all the track (title/duration/path) for a title informed by user
                cursor.execute("set schema 'guillaume_kevin'; SELECT track.title, track.duration, track.path FROM track WHERE track.title = '"+format(args.title[i][0])+"'");
                x = cursor.fetchall();
                shuffle(x)
                #convert the arg title duration into percent(/100) and multipliate by duration total informed by user
                durationArg = ((int(format(args.title[i][1]))/100)) * duration             
                durationArg = durationArg * 60 #convert duration in s (*60)
                print("duration in second for this arg = "+str(durationArg))
                timeUse = 0 
                j=0
                k=0
                while durationArg > timeUse:
                    #request for count number of track for this title
                    cursor.execute("set schema 'guillaume_kevin'; SELECT count(*) FROM track WHERE track.title = '"+format(args.title[i][0])+"'");
                    z=cursor.fetchone()
                    timeUse= timeUse + x[k][1]                                  
                    playList.append(x[k])#add track to playlist   
                    print(str(timeUse))
                    j=j+1
                    k=k+1
                    if k == z[0]:
                        k= 0
                print(" number of music with this title : "+str(j))#display the number of track for this argument
                i=i+1#permit to separate title 1/title 2/..
                
        #calculate rest
        print("========REST========")            
        h=0
        timeUseRest = 0
        durationRest = diffpercent/100 #percent of rest
        durationRest = durationRest * duration #percent * duration for know the duration in min
        durationRest = durationRest * 60 #percent in s for the rest
        print("durationRest : "+str(durationRest)) #print the rest
        cursor.execute("set schema 'guillaume_kevin'; SELECT title, duration, path FROM guillaume_kevin.track");
        rest = cursor.fetchall();
        shuffle(rest)#mix the result of request
        
        j=0
        k=0
        while durationRest > timeUseRest:
            #request in database all the track (title/duration/path)
            cursor.execute("set schema 'guillaume_kevin'; SELECT count(*) FROM guillaume_kevin.track")     
            result=cursor.fetchall()
            timeUseRest= timeUseRest+rest[h][1]  
            playList.append(rest[h])#add the track to playlist    
            print(str(timeUseRest))
            h=h+1
            k=k+1
            if k == result[0]:
                k= 0
            
            
        print(playList)#test for see the playlist finished
        
        if args.r!=False:#if the user use argument '--r'
            shuffle(playList)#will mix the playlist (before playlist will stay like this : 50%artist1, 20%artist2,..)
            print(playList)#test for see the playlist mixed and finished
        
        #m3u
        if args.m3u!=False:#if the user use argument "--m3u"
            file=open("playlist.m3u", "w")#use the file name playlist.m3u
            for track in playList:# for all the value in playlist
                file.write(track[2]+"\n")#write in file the path and return to the line
            
        #xspf
        if args.xspf!=False:#if the user use argument "--xspf"
            root =etree.Element('playlist')#create the root named playlist    
            trackList = etree.SubElement(root, 'trackList')
            etree.SubElement(root, 'title').text='Playlist'
            root.set("version", "1")         
            root.set("xmlns","http://xspf.org/ns/0/")
            for mytrack in playList:#for all tracks in playlist
                track = etree.SubElement(trackList, 'track')#create a field 'track'
                trackL = etree.SubElement(track, 'title').text=mytrack[0]#with fields 'title' into 'track'
                trackL = etree.SubElement(track, 'duration').text=str(mytrack[1])#with fields 'duration' into 'track'
                trackL = etree.SubElement(track, 'location').text='file://'+mytrack[2]#with fields 'location' into 'track'
                
            try:    #test for open the file 
                with open('playlist.xspf', 'w') as fic:         
                    fic.write(etree.tostring(root, xml_declaration=True, pretty_print=True, method="xml", encoding="UTF-8").decode('utf-8')) 
            except IOError:   #send a message error if the program can write  
                print('Error writing')     
                sys.exit(0)
        
        
    else:# if the percent == 100
        playList=[]
        #Check if the user use m3u/xspf/or both
        if args.m3u!=True:#if the argument --m3u isnt informed
            #print("no m3u")
            if args.xspf!=True:#if the argument --xspf isnt informed
                print("Please enter a format ! --m3u or --xspf")#m3u and xspf aren't informed so the program close
                sys.exit(0)
                
        if args.xspf!=True:#if the argument --xspf isnt informed
            #print("no xspf")
            if args.m3u!=True:#if the argument --m3u isnt informed
                print("Please enter a format ! --m3u or --xspf")#xspf and m3u aren't informed so the program close
                sys.exit(0)        
        
        #if the argument artist is different to default
        if args.artist!=None:
            print("=======ARTIST========")#separate fields
            i=0
            for arg in args.artist:#for all artist informed by user
                print("===")
                print("Artist "+str(i + 1))#separate artist(s)
                print("===")
                #request in database all the track (title/duration/path) for an artist informed by user
                cursor.execute("set schema 'guillaume_kevin'; SELECT track.title, track.duration, track.path FROM track INNER JOIN artist ON track.artist = artist.id WHERE artist.firstname = '"+format(args.artist[i][0])+"'");
                x = cursor.fetchall();
                #print(x)
                shuffle(x)#permit to mix element in a list (here the result of the request)
                #print(x)
                durationArg = ((int(format(args.artist[i][1]))/100)) * duration#convert the arg artist duration into percent(/100) and multipliate by duration total informed by user          
                durationArg = durationArg * 60 #convert duration in s (*60)
                print("duration in second for this arg = "+str(durationArg))#test for see if the arg duration is ok
                timeUse = 0 #initialize the time use for this argument
                j=0
                k=0
                while durationArg > timeUse:#while duration of this argument > timeUsed by tracks selected
                    #request for count number of track for this artist
                    cursor.execute("set schema 'guillaume_kevin'; SELECT count(*) FROM track INNER JOIN artist ON track.artist = artist.id WHERE artist.firstname = '"+format(args.artist[i][0])+"'");
                    z=cursor.fetchone()
                    timeUse= timeUse + x[k][1]#timeUse take value of the duration of the track(s)                                
                    playList.append(x[k])#add track to playlist
                    print(str(timeUse))#test for see the timeUse increase
                    j=j+1
                    k=k+1
                    if k == z[0]:#this condition is use when you don't have any tracks on playlist and need to restart
                        k= 0
                print(" number of music to the artist : "+str(j))#display the nomber of track for this argument
                i=i+1#permit to separate artist 1/artist 2/..
        
        #if the argument album is different to default
        if args.album!=None:
            print("=======ALBUM========")# similar as artist
            i=0
            for arg in args.album:
                print("===")
                print("Album "+str(i + 1))
                print("===")
                #request in database all the track (title/duration/path) for an album informed by user
                cursor.execute("set schema 'guillaume_kevin'; SELECT track.title, track.duration, track.path FROM track INNER JOIN album ON track.album = album.id WHERE album.title = '"+format(args.album[i][0])+"'");
                x = cursor.fetchall();
                shuffle(x)
                #convert the arg album duration into percent(/100) and multipliate by duration total informed by user
                durationArg = ((int(format(args.album[i][1]))/100)) * duration             
                durationArg = durationArg * 60 #convert duration in s (*60)
                print("duration in second for this arg = "+str(durationArg))
                timeUse = 0
                j=0
                k=0
                while durationArg > timeUse:
                    #request for count number of track for this album
                    cursor.execute("set schema 'guillaume_kevin'; SELECT count(*) FROM track INNER JOIN album ON track.album = album.id WHERE album.title = '"+format(args.album[i][0])+"'");
                    z=cursor.fetchone()
                    timeUse= timeUse + x[k][1]                                  
                    playList.append(x[k])#add track to playlist    
                    print(str(timeUse))
                    j=j+1
                    k=k+1
                    if k == z[0]:
                        k= 0
                print(" number of music to the album : "+str(j))#display the number of track for this argument
                i=i+1#permit to separate album 1/album 2/..
        
        #if the argument kind is different to default
        if args.kind!=None:
            print("=======KIND========")#similar as artist
            i=0
            for arg in args.kind:
                print("===")
                print("Kind "+str(i + 1))
                print("===")
                #request in database all the track (title/duration/path) for a kind informed by user
                cursor.execute("set schema 'guillaume_kevin'; SELECT track.title, track.duration, track.path FROM track INNER JOIN kind ON track.kind = kind.id WHERE kind.name = '"+format(args.kind[i][0])+"'");
                x = cursor.fetchall();
                shuffle(x)
                #convert the arg kind duration into percent(/100) and multipliate by duration total informed by user
                durationArg = ((int(format(args.kind[i][1]))/100)) * duration             
                durationArg = durationArg * 60 #convert duration in s (*60)
                print("duration in second for this arg = "+str(durationArg))
                timeUse = 0
                j=0
                k=0
                while durationArg > timeUse:
                    #request for count number of track for this kind
                    cursor.execute("set schema 'guillaume_kevin'; SELECT count(*) FROM track INNER JOIN kind ON track.kind = kind.id WHERE kind.name = '"+format(args.kind[i][0])+"'");
                    z=cursor.fetchone()
                    timeUse= timeUse + x[k][1]                                
                    playList.append(x[k])#add track to playlist
                    print(str(timeUse))
                    j=j+1
                    k=k+1
                    if k == z[0]:
                        k= 0
                print(" number of music with the kind : "+str(j))#display the number of track for this argument
                i=i+1#permit to separate kind 1/kind 2/..
        
        #if the argument title is different to default
        if args.title!=None:
            print("=======TITLE========")
            i=0
            for arg in args.title:
                print("===")
                print("Title "+str(i + 1))
                print("===")
                #request in database all the track (title/duration/path) for a title informed by user
                cursor.execute("set schema 'guillaume_kevin'; SELECT track.title, track.duration, track.path FROM track WHERE track.title = '"+format(args.title[i][0])+"'");
                x = cursor.fetchall();
                shuffle(x)
                #convert the arg title duration into percent(/100) and multipliate by duration total informed by user
                durationArg = ((int(format(args.title[i][1]))/100)) * duration             
                durationArg = durationArg * 60 #convert duration in s (*60)
                print("duration in second for this arg = "+str(durationArg))
                timeUse = 0 
                j=0
                k=0
                while durationArg > timeUse:
                    #request for count number of track for this title
                    cursor.execute("set schema 'guillaume_kevin'; SELECT count(*) FROM track WHERE track.title = '"+format(args.title[i][0])+"'");
                    z=cursor.fetchone()
                    timeUse= timeUse + x[k][1]                                  
                    playList.append(x[k])#add track to playlist   
                    print(str(timeUse))
                    j=j+1
                    k=k+1
                    if k == z[0]:
                        k= 0
                print(" number of music with this title : "+str(j))#display the number of track for this argument
                i=i+1#permit to separate title 1/title 2/..
                
        print(playList)#test for see the playlist finished
        
        if args.r!=False:#if the user use argument '--r'
            shuffle(playList)#will mix the playlist (before playlist will stay like this : 50%artist1, 20%artist2,..)
            print(playList)#test for see the playlist mixed and finished
        
        #m3u
        if args.m3u!=False:#if the user use argument "--m3u"
            file=open("playlist.m3u", "w")#use the file name playlist.m3u
            for track in playList:# for all the value in playlist
                file.write(track[2]+"\n")#write in file the path and return to the line
            
        #xspf
        if args.xspf!=False:#if the user use argument "--xspf"
            root =etree.Element('playlist')#create the root named playlist    
            trackList = etree.SubElement(root, 'trackList')
            etree.SubElement(root, 'title').text='Playlist'
            root.set("version", "1")         
            root.set("xmlns","http://xspf.org/ns/0/")
            for mytrack in playList:#for all tracks in playlist
                track = etree.SubElement(trackList, 'track')#create a field 'track'
                trackL = etree.SubElement(track, 'title').text=mytrack[0]#with fields 'title' into 'track'
                trackL = etree.SubElement(track, 'duration').text=str(mytrack[1])#with fields 'duration' into 'track'
                trackL = etree.SubElement(track, 'location').text='file://'+mytrack[2]#with fields 'location' into 'track'
                
            try:    #test for open and write the file 
                with open('playlist.xspf', 'w') as fic:         
                    fic.write(etree.tostring(root, xml_declaration=True, pretty_print=True, method="xml", encoding="UTF-8").decode('utf-8')) 
            except IOError:   #send a message error if the program can write  
                print('Error writing')     
                sys.exit(0)

if __name__ == '__main__':
    logger.debug("Execution of generator.py")
    main()
    logger.debug("End execution of generator.py")
else:
    logger.debug("Loading generator.py")
    
    
    
#https://docs.python.org/3/library/argparse.html#choices
#https://docs.python.org/3/howto/argparse.html#id1
#https://wiki.videolan.org/XSPF